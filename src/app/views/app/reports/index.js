import React, { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
    Input,
    Select,
    DatePickerField,
} from "../../../../../src/_metronic/_partials/controls";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../src/_metronic/_helpers/index";
import { InputGroup, Modal, OverlayTrigger, Tooltip ,Spinner} from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import { data } from './data'
 import overlayFactory from 'react-bootstrap-table2-overlay';
import "./index.css";
 import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Row, Col, Button } from "react-bootstrap";
import {FetchReport,PostTicket,FetchTicket,FetchTicketCustomer} from "../../../../redux/actions/ReportActions"
import {
    FetchTotalIncidents, FetchIncidentVolumes, FetchActivecasesbychannel, FetchSubchannel, FetchToptenLocation,
    FetchCategorlevelcount, FetchPublisherlevelcount
} from "../../../../redux/actions/DashboardActions";
import { connect } from 'react-redux'
import { useSelector, useDispatch } from 'react-redux';
// import CreateTicket from './CreateTicket';
import { SelectField } from "../../../../_metronic/_partials/controls/forms/SelectField"
import _ from 'lodash' ;
import TicketsList from '../tickets';
import FilterDrawer from '../shared-componets/filterdrawer';
const Reports = ({report_list}) => {
   
    const [show, setEditShow] = useState(false);
    const [create, setCreateShow] = useState(false);
    const handleCreateShow = () => setCreateShow(true);
    const handleEditShow = () => setEditShow(true);
    const packageName = localStorage.getItem('dpackage');
    const handleCreateClose = () => setCreateShow(false);
    const handleEditClose = () => setEditShow(false);
    const [selected,setSelected] = useState([]);
    const [ticketid,setTicketId] = useState();
    const [ticket_data,setTicket_data] = useState();

    const [panel, setPanel] = useState(true);
    const toggleDrawer = (e) => {
        setPanel(false)
    }
    const dispatch = useDispatch();
    const download = () => {
        //  dispatch(DownloadReport(packageName,'2020-01-10','2022-01-10','all','all','all','all','all','all','all'))
    }
    const bodyObj =    {
        // 'issue':{
            
        project_id:1,
        subject:"",
        description:"",
        due_date:"",
        assigned_to_id:"", 
        priority_id:"",
         status_id:1,
        // }

    }

    const handleSubmit = (data) => {
       
        // console.log('typeof', typeof data);
        dispatch(FetchTotalIncidents(data))
        dispatch(FetchIncidentVolumes(data))
        dispatch(FetchActivecasesbychannel(data))
        dispatch(FetchSubchannel(data))
        dispatch(FetchToptenLocation(data))
        dispatch(FetchCategorlevelcount(data))
        dispatch(FetchPublisherlevelcount(data))
    }

const checkLoading = packageName == 
    // const Assignee = _.filter(ticket_customer, function(item){
    //     console.log('items',    item.name.memberships.project);
    //     // return item.memberships.project ;
    //   });

   
    // var ticket_cus = ticket_customer.memberships
    
    // var assinee=[];
    // for(var i in ticket_cus){
    //     console.log("AAAA",ticket_cus[i].user.name)
    //     if(ticket_cus[i].user.name == undefined)
    //     assinee.push(ticket_cus[i].user.name)
    // }
    // console.log("assineeassinee$$$",assinee);
    

// console.log('assign',Assignee);
    useEffect(() => {
 
        dispatch(FetchReport(packageName,'2020-01-10','2022-01-10','all','all','all','all','all','all','all'))
        // dispatch(FetchTicketCustomer())
           
    }, [])

    const handlePriorityandStatus = (e) =>{
        
        setSelected(e.target.value);
    }

    const ViewTicket = (e) =>{
        console.log('right clic');
    }
    
function createTicket(e,id,status){
    console.log('id & ',id+" status"+status);
    e.preventDefault();
  
    if(id != 'null' && id)  {
        dispatch(FetchTicket(id))
    
        handleEditShow(true);
}

else{
    handleCreateShow(true);
    console.log('ticket id',id);
    setTicketId(id)
}

}
    console.log('reportsss',report_list);
    // console.log('ticket_customer',ticket_customer);
    const columns = [
        {
            dataField: "mfe_id",
            text: "ID",
            sort: true,
            style: {

                width: '10px'
            },
            headerStyle: {
                // backgroundColor: 'green',
                minWidth: '50px',
                width: '50px'
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis" style={{ width: '20px' }} >
                        <a onClick={(e)=>createTicket(e,row.ticket_id,row.ticket_status)} onContextMenu={(e)=>ViewTicket(e)} style={{ color: "#3699ff" }}> {row.mfe_id}</a>
                    </div>
                );
            }
        },
        {
            dataField: "inserted_date",
            text: "Date",
            sort: true,
            style: {
                width: '5%'
            },

            formatter: (cellContent, report_list) => {
                return (
                    <div className="text-ellipsis">
                        {/* <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.date}
                                </Tooltip>
                            }
                        > */}
                        <span>{report_list.inserted_date}</span>
                        {/* </OverlayTrigger> */}
                    </div>
                );
            }


        },
        {
            dataField: "sub_channel",
            text: "Sub Channel",
            sort: 'true',
            // 
            style: {
                width: '12%',


            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.sub_channel}
                                </Tooltip>
                            }
                        >
                            <span>{row.sub_channel}</span>
                        </OverlayTrigger>
                    </div>
                );
            }


        },
        {
            dataField: "sub_sub_channel",
            text: "Sub Channel",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.sub_sub_channel}
                                </Tooltip>
                            }
                        >
                            <span>{row.sub_sub_channel}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "channel_name",
            text: "Channel",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, report_list) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {report_list.channel_name}
                                </Tooltip>
                            }
                        >
                            <span>{report_list.channel_name}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "location",
            text: "Location",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.location}
                                </Tooltip>
                            }
                        >
                            <span>{row.location}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "brand",
            text: "Brand",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.brand}
                                </Tooltip>
                            }
                        >
                            <span>{row.brand}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "keyword_term",
            text: "Keyword",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, report_list) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {report_list.keyword}
                                </Tooltip>
                            }
                        >
                            <span>{report_list.keyword_term}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "country",
            text: "Country",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.country}
                                </Tooltip>
                            }
                        >
                            <span>{row.country}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "category",
            text: "Category",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.category}
                                </Tooltip>
                            }
                        >
                            <span>{row.category}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "sub_category",
            text: "Sub Category",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.sub_category}
                                </Tooltip>
                            }
                        >
                            <span>{row.sub_category}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "priority",
            text: "Priority",
            
            
            style: {
                width: '12%',
            },
            formatter: (cellContent, row,{value}) => {

                {
                    return (
                      <select
                        name="priority"
                        defaultValue={row.priority}
                  
                        onChange={(event) => {
                          handlePriorityandStatus(row.mfe_id, {
                            priority: event.target.value,
                          });
                        }}
                      >
                        <option value="High">High</option>
                        <option value="Mid">Mid</option>
                        <option value="Low">Low</option>
                      </select>
                    );
                  }
            }
           
        },
        {
            dataField: "status_update_date",
            text: "Status Update Date",
            sort: true,
            style: {
                width: '12%',
            },

            headerStyle: {

                minWidth: '150px',
                width: '150px'
            },
            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.status_update_date}
                                </Tooltip>
                            }
                        >
                            <span>{row.status_update_date}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "ticket_id",
            text: "Ticket ID",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.ticket_id}
                                </Tooltip>
                            }
                        >
                            <span>{row.ticket_id}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "ticket_status",
            text: "Ticket Status",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.ticket_status}
                                </Tooltip>
                            }
                        >
                            <span>{row.ticket_status}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "ad_heading",
            text: "Heading",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, report_list) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {report_list.ad_heading}
                                </Tooltip>
                            }
                        >
                            <span>{report_list.ad_heading}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "ad_description",
            text: "Description",
            sort: true,
            style: {
                width: '12%',
            },
            headerStyle: {
                // backgroundColor: 'green',
                minWidth: '50px',
                width: '50px'
            },
            formatter: (cellContent, report_list) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {report_list.ad_description}
                                </Tooltip>
                            }
                        >
                            <span>{report_list.ad_description}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "case_reports",
            text: "Case Report",
            sort: true,
            style: {
                width: '12%',
            },
            

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                     
                    <OverlayTrigger
                        placement="left"
                        overlay={
                            <Tooltip id={`tooltip-left`}>
                                {row.case_reports}
                            </Tooltip>
                        }
                    >
                          <a target={row.case_reports} href={row.case_reports}>
                    <span className="svg-icon svg-icon-md svg-icon-primary">
                  <SVG
                            src={toAbsoluteUrl(
                                "/media/svg/icons/General/link.svg"
                            )}
                        >       <span>{row.case_reports}</span></SVG>
                        </span>
                        </a>
                    </OverlayTrigger>
                </div>
                    );
            }
        },
        {
            dataField: "screenshot",
            text: "Screenshot",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                     
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.screenshot}
                                </Tooltip>
                            }
                        >
                              <a target={row.screenshot} href={row.screenshot}>
                        <span className="svg-icon svg-icon-md svg-icon-primary">
                      <SVG
                                src={toAbsoluteUrl(
                                    "/media/svg/icons/General/link.svg"
                                )}
                            >       <span>{row.screenshot}</span></SVG>
                            </span>
                            </a>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "destination_url",
            text: "Destination URL",
            sort: true,
            style: {
                width: "30%",
            },

            headerStyle: {

                minWidth: '150px',
                width: '150px'
            },
            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                     
                    <OverlayTrigger
                        placement="left"
                        overlay={
                            <Tooltip id={`tooltip-left`}>
                                {row.destination_url}
                            </Tooltip>
                        }
                    >
                          <a target={row.destination_url} href={row.destination_url}>
                    <span className="svg-icon svg-icon-md svg-icon-primary">
                  <SVG
                            src={toAbsoluteUrl(
                                "/media/svg/icons/General/link.svg"
                            )}
                        >       <span>{row.destination_url}</span></SVG>
                        </span>
                        </a>
                    </OverlayTrigger>
                </div>
                );
            }
        },
        {
            dataField: "ad_display_url",
            text: "Display URL",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                     
                    <OverlayTrigger
                        placement="left"
                        overlay={
                            <Tooltip id={`tooltip-left`}>
                                {row.ad_display_url}
                            </Tooltip>
                        }
                    >
                          <a target={row.ad_display_url} href={row.ad_display_url}>
                    <span className="svg-icon svg-icon-md svg-icon-primary">
                  <SVG
                            src={toAbsoluteUrl(
                                "/media/svg/icons/General/link.svg"
                            )}
                        >       <span>{row.ad_display_url}</span></SVG>
                        </span>
                        </a>
                    </OverlayTrigger>
                </div>
                );
            }
        },
        {
            dataField: "source_url",
            text: "Source URL",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.source_url}
                                </Tooltip>
                            }
                        >
                            <span>{row.source_url}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "destination_url_domain",
            text: "Destination URL Domain",
            // sort: true,
            style: {
                width: '22%',





            },
            headerStyle: {

                minWidth: '200px',
                width: '200px'
            },

            formatter: (cellContent, row) => {
                return (
                    // <div className="text-ellipsis">
                    <OverlayTrigger
                        placement="left"
                        overlay={
                            <Tooltip id={`tooltip-left`}>
                                {row.destination_url_domain}
                            </Tooltip>
                        }
                    >
                        <span>{row.destination_url_domain}</span>
                    </OverlayTrigger>
                    // </div>
                );
            }
        },
        {
            dataField: "publisher",
            text: "Publisher",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.publisher}
                                </Tooltip>
                            }
                        >
                            <span>{row.publisher}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "sub_publisher",
            text: "Sub Publisher",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.sub_publisher}
                                </Tooltip>
                            }
                        >
                            <span>{row.sub_publisher}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "campaign",
            text: "Campaign",
            sort: true,
            style: {
                width: '12%',
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.campaign}
                                </Tooltip>
                            }
                        >
                            <span>{row.campaign}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "campaign_name",
            text: "Campaign Name",
            sort: true,
            style: {
                width: '12%',
            },
            headerStyle: {

                minWidth: '140px',
                width: '150px'
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.campaign_name}
                                </Tooltip>
                            }
                        >
                            <span>{row.campaign_name}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "additional_info1",
            text: "Additional Info 1",
            sort: true,
            style: {
                width: '12%',
            },

            headerStyle: {

                minWidth: '140px',
                width: '150px'
            },
            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.additional_info1}
                                </Tooltip>
                            }
                        >
                            <span>{row.additional_info1}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "tat",
            text: "TAT",
            sort: true,
            style: {

                width: '10%',
                // width: '150px'
            },

            headerStyle: {

                minWidth: '40px',
                width: '50px'
            },
        },

        {
            dataField: "ticket_open_status",
            text: "Ticket Open Status",
            sort: true,
            style: {
                width: '12%',
            },
            headerStyle: {

                minWidth: '140px',
                width: '150px'
            },

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.ticket_open_status}
                                </Tooltip>
                            }
                        >
                            <span>{row.ticket_open_status}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },

        {
            dataField: "status",
            text: "Status",
            formatter: (cellContent, report_list) => {
                return (
                    <div>
                        {report_list.status === "Active" &&
                            <span class="label label-lg label-light-success label-inline">{report_list.status}</span>
                        }
                        {report_list.status === "Inprogress" &&
                            <span class="label label-lg label-light-warning label-inline">{report_list.status}</span>
                        }
                        {report_list.status === "Rejected" &&
                            <span class="label label-lg label-light-danger label-inline">{report_list.status}</span>
                        }
                        {report_list.status === "Approved" &&
                            <span class="label label-lg label-light-primary label-inline">{report_list.status}</span>
                        }
                    </div>
                );
            }
        }


    ];

    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total ml-3">
            Showing {from} to {to} of {size} Results
        </span>
    );

    const selectRow = {
        mode: "checkbox",
        clickToSelect: true,
        hideSelectAll: false,
    };

    const options = {
        paginationSize: 4,
        alwaysShowAllBtns: true, // Always show next and previous button
        withFirstAndLast: false, // Hide the going to First and Last page button
        // hideSizePerPage: true, // Hide the sizePerPage dropdown always
        // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
        firstPageText: 'First',
        prePageText: 'Back',
        nextPageText: 'Next',
        lastPageText: 'Last',
        nextPageTitle: 'First page',
        prePageTitle: 'Pre page',
        firstPageTitle: 'Next page',
        lastPageTitle: 'Last page',
        showTotal: true,
        paginationTotalRenderer: customTotal,
        disablePageTitle: true,
        sizePerPageList: [{
            text: '10', value: 10
        }, {
            text: '20', value: 20
        },
        {
            text: '30', value: 30
        },{
            text: '40', value: 40
        },
        {
            text: '50', value: 50
        },{
            text: '100', value: 100
        }
    ] // A numeric array is also available. the purpose of above example is custom the text
    };

    

    return (
        <>
            <div className="card card-custom">
                <Card>
                    {/* <CardHeader title="Tickets list"> */}
                    <CardHeaderToolbar>
                        <InputGroup >
                            {/* <input type="text" class="form-control" name="searchText" placeholder="Enter TicketID" />
                                <button className="btn btn-primary font-weight-bolder font-size-sm mr-3">
                                 <i class="fas fa-search"></i>
                                </button> */}
                            <Row style={{ marginTop: '14px', marginBottom: '-25px' }}>
                                <Col xs={6} sm={6} md={3} lg={1} xl={3} style={{ marginLeft: '32px', marginRight: '-14px' }}>
                                    <div class="input-group">
                                        <input class="form-control border-secondary py-2" type="search" label='Enter ID' />
                                        <div class="input-group-append">
                                            <button class="btn btn-primary font-weight-bolder font-size" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={6} sm={6} md={3} lg={1} xl={3} style={{ marginRight: '-8px' }}>
                                    <div class="input-group mw-100">
                                        <input class="form-control border-secondary py-2" type="search" label='Ticket ID' />
                                        <div class="input-group-append">
                                            <button class="btn btn-primary font-weight-bolder font-size" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={6} sm={6} md={1} lg={1} xl={2} style={{ marginRight: '-35px' }}>
                                    <button className="btn btn-primary font-weight-bolder font-size">
                                        Close Ticket
                                    </button>
                                </Col>
                                <Col xs={6} sm={6} md={1} lg={1} xl={2} style={{ marginRight: '-24px' }}>
                                    <button className="btn btn-primary font-weight-bolder font-size" onClick={handleEditShow}>
                                        Create Ticket
                                    </button>
                                </Col>
                                <Col xs={6} sm={6} md={1} lg={1} xl={1}>
                                    <button className="btn btn-primary font-weight-bolder font-size">
                                        Save
                                    </button>
                                </Col>
                                <Col xs={6} sm={6} md={1} lg={1} xl={1}>
                                    <button className="btn btn-primary font-weight-bolder font-size" onClick={()=>download()}>
                                        Download
                                    </button>
                                </Col>
                            </Row>
                        </InputGroup>

                    </CardHeaderToolbar>
                    {/* </CardHeader> */}
                    {/* <CardBody>
                        <div className="App"> */}
                            {/* { props => (          <ToolkitProvider
  keyField="id"
  data={ data }
  columns={ columns }
  search
> */}

<CardBody >
                        {
                        report_list.length === 0 ?
                            <div className="text-center p-5 mt-5">
                                <Spinner animation="border" variant="warning" />
                            </div>
 :
 <div className="App">
                            <BootstrapTable
                                bootstrap4
                                keyField="mfe_id"
                                data={report_list}
                                columns={columns}
                                pagination={paginationFactory(options)}
                                wrapperClasses="table-responsive"
                                classes="table table-vertical-center header-class"
                                bordered={false}
                                selectRow={selectRow}
                                condensed
                                responsive
                            
                                headerClasses="header-class"
                                striped
                                hover
                              
                            />
                               
                            {/* </ToolkitProvider>)} */}
                            </div>
                        } 
                      
                    </CardBody>
                </Card>
{ 
                <Modal show={create} onHide={handleCreateClose} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>Create  Tickets</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Formik
                            initialValues={bodyObj}
                         
                            onSubmit={values => {
                                // same shape as initial values
                                const data={
                                    "issue" : {
                                            "assigned_to_id":287,
                                  "due_date":"2023-10-05",
                                      "project_id": 1,
                                  "subject":values.subject,
                                  "description":values.description,
                                  "priority_id": 1,
                                  "status_id":1}
                                //   'project_id':1,
                                //     'subject':values.subject,
                                //    'description':values.description,
                                //     'due_date':'2022-05-12',
                                //     'assigned_to_id':287, 
                                //     'priority_id':values.priority_id,
                                //     'status_id':'1'
                                }
                                console.log(data)
                                // let issue=
                                // {
                                //     "issue": {
                                //         "assigned_to_id":287,
                                //   "due_date":"2022-06-05",
                                //       "project_id": 1,
                                //   "subject": "TEST CREATE",
                                //   "description":"test create issue",
                                //   "priority_id": 1,
                                //   "status_id":1
                                  
                                      
                                //     }
                                //   }
                                // console.log('issue',typeof issue);
                                dispatch(PostTicket(data))
                                // setTicket_data(values)
                                // console.log('kllnkn',values);
                            }}
                        >
                            {({ errors, touched }) => (
                                <Form className="form form-label-right">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <Field name="subject"
                                                component={Input}
                                                placeholder="Subject"
                                                label="Subject" />
                                        </div>

                                        <div className="col-md-6">
                                            <DatePickerField
                                                name="due_date"
                                                label="Due Date"
                                            />
                                        </div>

                                        <div className="col-md-6">
                                            <Select name="priority_id" label="Priority">
                                                <option value="1">Low</option>
                                                <option value="2">Normal</option>
                                                <option value="3">High</option>
                                                <option value="4">Urgent</option>
                                                <option value="5">Immediate</option>
                                            </Select>
                                        </div>
                                        <div className="col-md-6">
                                            <Select name="status_id" label="Status">
                                                <option value="low">Low</option>
                                                <option value="normal">Normal</option>
                                                <option value="high">High</option>
                                                <option value="urgent">Urgent</option>
                                                <option value="immediate">Immediate</option>
                                            </Select>
                                        </div>

                                        <div className="col-md-6">
                                            <Select name="assigned_to_id" label="Assignee">
                                                <option value="mohitdayal">Mohit Dayal</option>
                                                <option value="giriprasadsanka">Giri Prasad Sanka</option>
                                                <option value="ankushsharma">Ankush Sharma</option>
                                                <option value="kiranar">Kiran AR</option>
                                                <option value="mfilteritteam">mFilterIt Team</option>
                                            </Select>
                                        </div>


                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label>Description</label>
                                                <Field
                                                    name="description"
                                                    as="textarea"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label>Add Notes</label>
                                                <Field
                                                    name="note"
                                                    as="textarea"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>

                                    </div>
                                    <Modal.Footer className="pb-0 pr-0">
                                        <button className="btn btn-light btn-elevate" onClick={handleCreateClose}>
                                            Close
                                        </button>
                                        <button type="submit" className="btn btn-primary font-weight-bolder
                                        font-size-sm mr-3"
                                         >Submit</button>
                                    </Modal.Footer>
                                </Form>
                            )}
                        </Formik>
                    </Modal.Body>

                </Modal>
}           
                <Modal show={show} onHide={handleEditClose} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Tickets</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Formik
                            initialValues={{
                                subject: '',
                                due_date: '',
                                status: '',
                                priority: '',
                                assignee: '',
                                description: '',
                                notes: '',
                            }}
                            onSubmit={values => {
                                // same shape as initial values
                                console.log(values);
                            }}
                        >
                            {({ errors, touched }) => (
                                <Form className="form form-label-right">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <Field name="subject"
                                                component={Input}
                                                placeholder="Subject"
                                                label="Subject" />
                                        </div>

                                        <div className="col-md-6">
                                            <DatePickerField
                                                name="due_date"
                                                label="Due Date"
                                            />
                                        </div>

                                        <div className="col-md-6">
                                            <Select name="priority" label="Priority">
                                                <option value="low">Low</option>
                                                <option value="normal">Normal</option>
                                                <option value="high">High</option>
                                                <option value="urgent">Urgent</option>
                                                <option value="immediate">Immediate</option>
                                            </Select>
                                        </div>

                                        <div className="col-md-6">
                                            <Select name="assignee" label="Assignee">
                                                <option value="mohitdayal">Mohit Dayal</option>
                                                <option value="giriprasadsanka">Giri Prasad Sanka</option>
                                                <option value="ankushsharma">Ankush Sharma</option>
                                                <option value="kiranar">Kiran AR</option>
                                                <option value="mfilteritteam">mFilterIt Team</option>
                                            </Select>
                                        </div>

                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Description</label>
                                                <Field
                                                    name="description"
                                                    as="textarea"
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>

                                    </div>
                                    <Modal.Footer className="pb-0 pr-0">
                                        <button className="btn btn-light btn-elevate" onClick={handleEditClose}>
                                            Close
                                        </button>
                                        <button type="submit" className="btn btn-primary font-weight-bolder
                                        font-size-sm mr-3">Submit</button>
                                    </Modal.Footer>
                                </Form>
                            )}
                        </Formik>
                    </Modal.Body>

                </Modal>
                <FilterDrawer panel={panel} toggleDrawer={toggleDrawer} onSubmit={handleSubmit} />
            </div>

        </>
    )
}

const mapStateToProps = (state) => {
    const { report, } = state
    console.log('statestatestate',state);
    return {
      report_list:
      report && report.report_list ? report.report_list : [],
    //   ticket_customer:
    //   report && report.ticket_customer ? report.ticket_customer : [],
    }
  }
const mapDispatchToProps = channels => {
// return {...channels}
}

export default connect(mapStateToProps)(Reports) 