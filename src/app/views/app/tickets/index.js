import React, { useState, useEffect } from 'react'
import {
    Card,
    CardBody,
    CardHeader,
    CardHeaderToolbar,
    Input,
    Select,
    DatePickerField,
} from "../../../../_metronic/_partials/controls";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../src/_metronic/_helpers/index";
import { InputGroup, Modal, OverlayTrigger, Tooltip, Spinner } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import {
    FetchTicketIncidents , FetchTicketSearchIncidents
} from "../../../../redux/actions/TicketAction";    
import { useSelector, useDispatch } from 'react-redux';
import TicketModel from "./TicketModel"
import { connect } from 'react-redux'
import {FetchTicket} from "../../../../redux/actions/ReportActions"
const customTotal = (from, to, size) => (
    <span className="react-bootstrap-table-pagination-total ml-3">
        Showing {from} to {to} of {size} Results
    </span>
);

const options = {
    paginationSize: 4,
    alwaysShowAllBtns: true, // Always show next and previous button
    withFirstAndLast: false, // Hide the going to First and Last page button
    // hideSizePerPage: true, // Hide the sizePerPage dropdown always
    // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
    firstPageText: 'First',
    prePageText: <i class="ki ki-bold-arrow-back icon-xs"></i>,
    nextPageText: <i class="ki ki-bold-arrow-next icon-xs"></i>,
    lastPageText: 'Last',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
    showTotal: true,
    paginationTotalRenderer: customTotal,
    disablePageTitle: true,
    sizePerPageList: [
        { text: '5', value: 5 },
        { text: '10', value: 10 },
        { text: '20', value: 20 }
    ]
};

export default function  TicketsList() {

    const [ticketsData, setTicketData] = useState([]);
    const [show, setEditShow] = useState(false);
    const [viewshow, setViewtShow] = useState(false);
    const [singleRow, setSinglerow] = useState();

    const handleViewClose = () => setViewtShow(false);
    const handleViewShow = () => setViewtShow(true);

    const handleEditClose = () => setEditShow(false);
    const handleEditShow = () => setEditShow(true);

    const [confirmModalShow, setConfirmModalShow] = useState(false);
    const [ticketviewdata, setTicketviewdata] = useState(null);
    const [ticks, setTicks] = useState("");
    const[ticket_id,setTicket_id] = useState();
    const[value,setValue] = useState('');
    const dispatch = useDispatch();
console.log('value',value);
    const data = {
        "package_name": "in.itcstore",
        "fromDate": "2020-01-10",
        "toDate": "2022-01-10",
        "country": 'all',
        "category": 'all',
        "publisher": 'all',
        "channel": 'all',
        "brand": 'all',
        "status": 'all',
        "priority": 'all'
    }

    useEffect(() => {
        dispatch(FetchTicketIncidents(data))
        // dispatch(FetchTicketSearchIncidents("11786"))

    }, [])

    const incident_data = useSelector(state => state.tickets.incident_data)
    // const incident_data = useSelector(state => state.tickets.incident_data)
    const ticket_data = useSelector(state => state.report.ticket_details)
    console.log('ticket_data',ticket_data);

    // const [incidentData, setIncidentdata] = useState(incidentData!==undefined? incidentData.issues : null); // set campaign as default
    // console.log(incidentData)

    const openPopup = (e,data) => {
        e.preventDefault(e);        
         setValue(e.target.value);

         console.log('ticket_vxvxdata',data);
      
        setViewtShow(true);
        dispatch(FetchTicket(value))
        setSinglerow(data);
        console.log('valll',value);
    
       
    }
   

    const columns = [

        {
            dataField: "id",
            text: "Ticket ID",
            sort: true,
            style: {
                width: '8%'
            },
            formatter: (cellContent, row) => {


                return (
                    <div>
                        <a onClick={handleEditShow} style={{ color: "#3699ff" }}>{row.id}</a>
                        {/* {row.id} */}

                    </div>
                );
            }
        },

        {
            dataField: "subject",
            text: "subject",

            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis-50">
                        <OverlayTrigger
                            placement="top"
                            overlay={
                                <Tooltip id={`tooltip-top`}>
                                    {row.subject}
                                </Tooltip>
                            }
                        >
                            <span>{row.subject}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "description",
            text: "Description",
            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis">
                        <OverlayTrigger
                            placement="left"
                            overlay={
                                <Tooltip id={`tooltip-left`}>
                                    {row.description}
                                </Tooltip>
                            }
                        >
                            <span>{row.description}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "priority.name",
            text: "Priority",
            sort: true,
            formatter: (cellContent, row) => {
                console.log(row.priority.name)
                return (
                    <div className="text-ellipsis-50">
                        <OverlayTrigger
                            placement="top"
                            overlay={
                                <Tooltip id={`tooltip-top`}>
                                    {row.priority.name}
                                </Tooltip>
                            }
                        >
                            {/* <span>{row.priority.name}</span> */}
                            <span>{row.priority.name}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "status.name",
            text: "Status",
            sort: true,
            formatter: (cellContent, row) => {
                return (
                    <div>
                        {row.status.name === "New" ?
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id={`tooltip-top`}>
                                        {row.status.name}
                                    </Tooltip>
                                }
                            >
                                <span class="label label-lg label-light-success label-inline">{row.status.name}</span>
                            </OverlayTrigger>
                            :
                            <OverlayTrigger
                                placement="top"
                                overlay={
                                    <Tooltip id={`tooltip-top`}>
                                        {row.status.name}
                                    </Tooltip>
                                }
                            >
                                <span class="label label-lg label-light-warning label-inline">{row.status.name}</span>
                            </OverlayTrigger>}
                    </div>
                );
            }
        },
        {
            dataField: "assigned_to.name",
            text: "Assignee",
            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis-50">
                        <OverlayTrigger
                            placement="top"
                            overlay={
                                <Tooltip id={`tooltip-top`}>
                                    {/* {row.assigned_to.name}     */}
                                    {row.assigned_to && row.assigned_to.name}
                                </Tooltip>
                            }
                        >
                            <span>{row.assigned_to && row.assigned_to.name}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "author.name",
            text: "Author",
            formatter: (cellContent, row) => {
                return (
                    <div className="text-ellipsis-50">
                        <OverlayTrigger
                            placement="top"
                            overlay={
                                <Tooltip id={`tooltip-top`}>
                                    {row.author.name}
                                </Tooltip>
                            }
                        >
                            <span>{row.author.name}</span>
                        </OverlayTrigger>
                    </div>
                );
            }
        },
        {
            dataField: "start_date",
            text: "Start Date",
            style: {
                width: '12%'
            }
        },
        {
            dataField: "due_date",
            text: "Due Date",
            style: {
                width: '10%'
            }
        },
        {
            dataField: "attachments",
            text: "Attach",
            formatter: (cellContent, row) => {
                return (
                    <div className="text-center">
                        {row.attachments}
                        {/* <a target="blank" href={row.attachments}> */}
                        {/* <span className="svg-icon svg-icon-md svg-icon-primary">
                                <SVG
                                    src={toAbsoluteUrl(
                                        "/media/svg/icons/Files/File.svg"
                                    )}
                                ></SVG>
                            </span> */}
                        {/* </a> */}
                    </div>
                );
            }
        },
        {
            text: "",
            style: {
                width: '10%'
            },
            formatter: (cellContent, row) => {
               
                return (
                    <div class="btn-toolbar" role="toolbar" aria-label="...">
                        <div class="btn-group mr-2" role="group" aria-label="...">
                            <button type="button" onClick={handleEditShow}
                                class="btn btn-outline-secondary btn-icon btn btn-icon btn-light">
                                <span className="svg-icon svg-icon-md svg-icon-primary">
                                    <SVG
                                        src={toAbsoluteUrl(
                                            "/media/svg/icons/Communication/Write.svg"
                                        )}
                                    ></SVG>
                                </span>
                            </button>
                            <button type="button" onClick={(e) => openPopup(e, row)}

                                class="btn btn-outline-secondary btn-icon btn btn-icon btn-light">
                                <span className="svg-icon svg-icon-md svg-icon-primary">
                                    <SVG
                                        src={toAbsoluteUrl("/media/svg/icons/Home/Eye.svg")}>
                                    </SVG>
                                </span>
                            </button>
                        </div>
                    </div>
                );
            }
        },
    ]

    return (

        <>

            <div className="card card-custom">

                <Card>
                    <CardHeader title="Tickets list">
                        <CardHeaderToolbar>
                            <InputGroup className="mb-3">
                                <input type="text" class="form-control" name="searchText" placeholder="Enter TicketID"  value={value}
                              
                                onChange={(event)=>setValue(event.target.value)}/>
                                <button className="btn btn-primary font-weight-bolder font-size-sm mr-3" 
                                   
                               onClick={(e) => openPopup(e, ticket_data)}
                            
                               >
                                    Search
                                </button>
                            </InputGroup>
                        </CardHeaderToolbar>
                    </CardHeader>

                    <CardBody className="pt-0 pb-0">
                        {incident_data.length === 0 ?
                            <div className="text-center p-5 mt-5">
                                <Spinner animation="border" variant="warning" />
                            </div>
                            :
                            <div className="App">
                                <BootstrapTable
                                    bootstrap4
                                    keyField="id"
                                    data={incident_data.issues}
                                    columns={columns}
                                    pagination={paginationFactory(options)}
                                    wrapperClasses="table-responsive"
                                    classes="table table-head-custom table-vertical-center overflow-hidden"
                                    bordered={false}
                                    condensed
                                    responsive
                                />
                            </div>
                        }
                    </CardBody>
                </Card>
            </div>
           
                <TicketModel
                    show={viewshow}
                    handleClose={() => handleViewClose(false)}
                    singleRow={singleRow}
                />
        

            <Modal show={show} onHide={handleEditClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Edit Ticket</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Formik
                        initialValues={{
                            subject: '',
                            due_date: '',
                            status: '',
                            priority: '',
                            assignee: '',
                            description: '',
                            notes: '',
                        }}
                        onSubmit={values => {
                            // same shape as initial values
                            console.log(values);
                        }}
                    >
                        {({ errors, touched }) => (
                            <Form className="form form-label-right">
                                <div className="row">
                                    <div className="col-md-4">
                                        <Field name="subject"
                                            component={Input}
                                            placeholder="Subject"
                                            label="Subject" />
                                    </div>

                                    <div className="col-md-4">
                                        <DatePickerField
                                            name="due_date"
                                            label="Due Date"
                                        />
                                    </div>

                                    <div className="col-md-4">
                                        <Select name="status" label="Status">
                                            <option value="new">New</option>
                                        </Select>
                                    </div>

                                    <div className="col-md-6">
                                        <Select name="priority" label="Priority">
                                            <option value="low">Low</option>
                                            <option value="normal">Normal</option>
                                            <option value="high">High</option>
                                            <option value="urgent">Urgent</option>
                                            <option value="immediate">Immediate</option>
                                        </Select>
                                    </div>

                                    <div className="col-md-6">
                                        <Select name="assignee" label="Assignee">
                                            <option value="mohitdayal">Mohit Dayal</option>
                                            <option value="giriprasadsanka">Giri Prasad Sanka</option>
                                            <option value="ankushsharma">Ankush Sharma</option>
                                            <option value="kiranar">Kiran AR</option>
                                            <option value="mfilteritteam">mFilterIt Team</option>
                                        </Select>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label>Description</label>
                                            <Field
                                                name="description"
                                                as="textarea"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label>Notes</label>
                                            <Field
                                                name="notes"
                                                as="textarea"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                </div>
                                <Modal.Footer className="pb-0 pr-0">
                                    <button className="btn btn-light btn-elevate" onClick={handleEditClose}>
                                        Close
                                    </button>
                                    <button type="submit" className="btn btn-primary font-weight-bolder
                                        font-size-sm mr-3">Submit</button>
                                </Modal.Footer>
                            </Form>
                        )}
                    </Formik>
                </Modal.Body>

            </Modal>



            {/* <Modal show={viewshow} onHide={handleViewClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Ticket Information</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-md-4">
                            <h6>Subject</h6>
                            <span>Take down</span>
                        </div>
                        <div className="col-md-4">
                            <h6>Priority</h6>
                            <span>Immediate</span>
                        </div>
                        <div className="col-md-4">
                            <h6>Project</h6>
                            <span>RBL Brand Infringement</span>
                        </div>
                        <div className="col-md-4">
                            <h6>Status</h6>
                            <span>New</span>
                        </div>
                        <div className="col-md-4">
                            <h6>Start Date</h6>
                            <span>2022-03-29</span>
                        </div>
                        <div className="col-md-4">
                            <h6>Due Date</h6>
                            <span>2022-03-29</span>
                        </div>

                        <div className="col-md-12">
                            <h6>Description</h6>
                            <span>Mentioned number in the given link does not belongs to RBL hence please take down the same.</span>
                        </div>
                    </div>
                </Modal.Body>
            </Modal> */}
        </>


    )
}

// const mapStateToProps = (state) => {
//     const { issues } = state
//     console.log('statestatestate',state);
//     return {
//       ticket_lists:
//       report && report.ticket_lists ? report.ticket_lists : [],
     
//     }
//   }
// const mapDispatchToProps = channels => {
// // return {...channels}
// }

// export default connect(mapStateToProps)(TicketsList) 