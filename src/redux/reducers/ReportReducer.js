import * as constant from "../constants/ReportConstants";
const initialState = {
  report_list:[],
  create_ticket:[],
  ticket_details:[],
  ticket_customer:[]
};

const ReportReducer = (state = initialState, { type, response }) => {
    switch (type) {



        case constant.REPORT_INIT:
  
            return {
                ...state,
                loading: true,
            };

        case constant.REPORT_INIT_SUCCESS:
   
            return {
                ...state,
                loading: false,
                report_list: response,
            };

        case constant.REPORT_INIT_ERROR:
   
            return {
                ...state,
                loading: false,
                error: response,
            };





        case constant.TICKET_DETAILS_INIT:
  
            return {
                ...state,
                loading: true,
            };

        

        case constant.TICKET_DETAILS_SUCCESS:
            console.log('TICKET_DETAILS_SUCCESS',response);
            return {
                 ...state,
                loading: false,
                ticket_details: response,
            };

            case constant.TICKET_DETAILS_ERROR:
                console.log('TICKET_DETAILS_ERROR',response);
                return {
                    ...state,
                    loading: false,
                    error: response,
                };




            case constant.TICKET_CUSTOMER_INIT:

            console.log('TICKET_CUSTOMER_INIT',response);
                return {
                    ...state,
                    loading: false,
                    ticket_customer: response,
                };

                case constant.TICKET_CUSTOMER_SUCCESS:
                    console.log('TICKET_CUSTOMER_SUCCESS',response);
                    return {
                        ...state,
                loading: false,
                ticket_customer: response,
                    };
                    case constant.TICKET_CUSTOMER_ERROR:
                        console.log('TICKET_CUSTOMER_ERROR',response);
                        return {
                            ...state,
                            loading: false,
                            error: response,
                        };








        case constant.CREATE_TICKET_INIT:
  console.log('CREATE_TICKET_INIT',response);
            return {
                ...state,
                loading: true,
            };

        case constant.CREATE_TICKET_INIT_SUCCESS:
   console.log('CREATE_TICKET_INIT_SUCCESS',response);
            return {
                ...state,
                loading: false,
                create_ticket: response,
            };

        case constant.CREATE_TICKET_INIT_ERROR:
            console.log('CREATE_TICKET_INIT_ERROR',response);
            return {
                ...state,
                loading: false,
                error: response,
            };



        default:
            return state;
    }
}
export default ReportReducer