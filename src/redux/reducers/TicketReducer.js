import * as constant from "../constants/TicketConstants";
const initialState = {
    incident_data: [],
    incidentloading: true,
    incidentdata_error: null,

    search_incident_data: [],
    search_incidentloading: true,
    search_incidentdata_error: null,
};

const TicketpagReducer = (state = initialState, { type, response }) => {
    switch (type) {
        case constant.TICKET_INCIDENTS:
            return {
                ...state,
                incidentloading: true,
            };

            case constant.TICKETSEARCH_INCIDENTS:
            return {
                ...state,
                search_incidentloading: true,
            };

        case constant.TICKET_INCIDENTS_SUCCESS:
            return {
                ...state,
                incidentloading: false,
                incident_data: response.issues,
            };

            case constant.TICKETSEARCH_INCIDENTS_SUCCESS:
            return {
                ...state,
                search_incidentloading: false,
                search_incident_data: response.issues,
            };

        case constant.TICKET_INCIDENTS_ERROR:
            return {
                ...state,
                incidentloading: false,
                incidentdata_error: response,
            };

            case constant.TICKETSEARCH_INCIDENTS_ERROR:
                return {
                    ...state,
                    search_incidentloading: false,
                    search_incidentdata_error: response,
                };

        default:
            return state;
    }
}

export default TicketpagReducer