import { EndPoints, ApiUrl } from '../helpers/Endpoints';

const TicketServices = {}

TicketServices.ViewTicketincidents = function(queryParams) {
    let searchParams = new URLSearchParams()
    Object.keys(queryParams).forEach(key => searchParams.append(key, queryParams[key]));
    let url = ApiUrl + EndPoints.ticket + "?" + searchParams
    //  + "?" + searchParams

    console.log("cscscsccscscs",searchParams)
    return fetch(url)
         .then(async response => {  
            const data = await response.json(); 
            return data;
            if (!response.ok) {
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
            return data;
        })
        .catch(error => {
            console.error('There was an error!', error);
        });
}

TicketServices.ViewTicketSearchincidents = function(queryParams) {
    
    
    let url = ApiUrl + EndPoints.search_ticket + "/" + queryParams
   

    return fetch(url)
         .then(async response => {  
            const data = await response.json(); 
            return data;
            if (!response.ok) {
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
            return data;
        })
        .catch(error => {
            console.error('There was an error!', error);
        });
}

export default TicketServices
