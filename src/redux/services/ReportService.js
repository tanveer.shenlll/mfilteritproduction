/* eslint-disable no-unreachable */
import { EndPoints, ApiUrl } from '../helpers/Endpoints';
import axios from 'axios'
const ReportServices = {}

ReportServices.ViewReportlist = function (packageName,fromDate,toDate,country,publisher,channel,brand,status,priority,category) {
    let url = ApiUrl + EndPoints.report_list + '?package_name=' + packageName + '&fromDate='+ fromDate +'&toDate=' + toDate +'&country=' + country + '&publisher=' + publisher + '&channel=' + channel + '&brand=' + brand + '&status=' + status + '&priority=' + priority + '&category=' + category
    // console.log('urlsssss',url);
    return fetch(url)
        .then(async response => {
            const data = await response.json();
            // console.log('dataaa',data);
            return data;
            if (!response.ok) {
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
        })
        .catch(error => {
            console.error('There was an error!', error);
        });
}

ReportServices.GetTicketdetails = function (ticket_id) {
    let url = ApiUrl + EndPoints.ticket_detail + ticket_id
     console.log('urlsssss',url);
    return fetch(url)
        .then(async response => {
            const data = await response.json();
             console.log('dataaa',data);
            return data 
            if (!response.ok) {
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
        })
        .catch(error => {
            console.error('There was an error!', error);
        });
}
ReportServices.GetTicketCustomer = function () {
    let url = ApiUrl + EndPoints.ticket_customer 
    //  console.log('GetTicketCustomer',url);
    return fetch(url)
        .then(async response => {
            const dataRes =  await response.json();
             console.log('dataaa',dataRes.data);
             
            return dataRes.data 
            if (!response.ok) {
                const error = (dataRes && dataRes.message) || response.statusText;
                return Promise.reject(error);
            }
        })
        .catch(error => {
            console.error('There was an error!', error);
        });
}

ReportServices.GetPriority = function () {
    let url = ApiUrl + EndPoints.priority 
     console.log('GetPriority',url);
    return fetch(url)
        .then(async response => {
            const data = await response.json();
             console.log('dataaa',data);
            return data 
            if (!response.ok) {
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
        })
        .catch(error => {
            console.error('There was an error!', error);
        });
}

ReportServices.Createticket = function async (body) 
  {
      console.log('sdasad',body);
    const requestOptions = {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'application/json',"Access-Control-Allow-Origin": "*", },
        body: JSON.stringify(body), 
    }
     let url = ApiUrl + EndPoints.create_ticket 
    return  fetch(url, requestOptions)
    .then(async response => {
        const data=await response.json()
        
        console.log('data data',data);
return data
      
        if (!response.ok) {
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
        }

    })
    .catch(error => {
        console.error('There was an error!', error);
    });   


    
}




export default ReportServices