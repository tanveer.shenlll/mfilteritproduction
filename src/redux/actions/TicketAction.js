import * as constant from '../constants/TicketConstants';

export const FetchTicketIncidents = (ticketdata) =>{
    return{
        type: constant.TICKET_INCIDENTS,
        ticketdata
    }
 };

 export const FetchTicketSearchIncidents = (ticketsearchdata) =>{
    return{
        type: constant.TICKETSEARCH_INCIDENTS,
        ticketsearchdata
    }
 };
 
 
