import { takeLatest, put, call } from "redux-saga/effects";
import * as constant from "../constants/ReportConstants";
import ReportServices from "../services/ReportService";

export function* ViewReportSaga(payload) {
    try {
        const response = yield call(ReportServices.ViewReportlist, payload.packageName,payload.fromDate,payload.toDate,payload.country,payload.country,payload.publisher,payload.channel,payload.brand,payload.status,payload.priority,payload.category);
        // console.log('resssss in saga report',response);
        yield put({ type: constant.REPORT_INIT_SUCCESS, response })
    } catch (error) {
        yield put({ type: constant.REPORT_INIT_ERROR, error })
    }
}
export function* CreateTicketSaga(payload) {
    try {
        const response = yield call(ReportServices.Createticket, payload.body);
        console.log('resssss in saga report',response);
        yield put({ type: constant.CREATE_TICKET_INIT_SUCCESS, response })
    } catch (error) {
        yield put({ type: constant.CREATE_TICKET_INIT_ERROR, error })
    }
}

export function* GetTicketdetails(payload) {
    try {
        const response = yield call(ReportServices.GetTicketdetails,payload.ticket_id);
         console.log('resssss in saga report',response);
        yield put({ type: constant.TICKET_DETAILS_SUCCESS, response })
    } catch (error) {
        yield put({ type: constant.TICKET_DETAILS_ERROR, error })
    }
}


export function* GetTicketCustomer() {
    try {
        const response = yield call(ReportServices.GetTicketCustomer );
         console.log('resssss in saga report',response);
        yield put({ type: constant.TICKET_CUSTOMER_SUCCESS, response })
    } catch (error) {
        yield put({ type: constant.TICKET_CUSTOMER_ERROR, error })
    }
}

export default function* ReportSaga() {
    yield takeLatest(constant.REPORT_INIT, ViewReportSaga);
    yield takeLatest(constant.CREATE_TICKET_INIT, CreateTicketSaga);
    yield takeLatest(constant.TICKET_DETAILS_INIT, GetTicketdetails);
    yield takeLatest(constant.TICKET_CUSTOMER_INIT,GetTicketCustomer );

}