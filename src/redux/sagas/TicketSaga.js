import { takeLatest, put, call } from "redux-saga/effects";
import * as constant from "../constants/TicketConstants";
import TicketServices from "../services/TicketServices";

export function* ViewTicketIncidentsSaga(payload) {
    try {
        const response = yield call(TicketServices.ViewTicketincidents ,payload.ticketdata);
        yield put({ type: constant.TICKET_INCIDENTS_SUCCESS,response })
    } catch (error) {
        yield put({ type: constant.TICKET_INCIDENTS_ERROR,error })
    }
}

export function* ViewTicketSearchincidents(payload) {
    try {
        const response = yield call(TicketServices.ViewTicketSearchincidents ,payload.ticketsearchdata);
        yield put({ type: constant.TICKETSEARCH_INCIDENTS_SUCCESS,response })
    } catch (error) {
        yield put({ type: constant.TICKETSEARCH_INCIDENTS_ERROR,error })
    }
}

export default function* TicketSaga() {
    yield takeLatest(constant.TICKET_INCIDENTS, ViewTicketIncidentsSaga);
    yield takeLatest(constant.TICKETSEARCH_INCIDENTS, ViewTicketSearchincidents);


}


